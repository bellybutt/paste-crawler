DB_FILE_PATH = "pastes_db.json"

PASTEBIN_URL = "https://pastebin.com"
ARCHIVE_URL = f"{PASTEBIN_URL}/archive"

EMPTY_USERNAMES = ["Guest", "Anonymous", "Unknown"]
EMPTY_TITLES = ["Untitled"]

SECONDS_TO_SLEEP = 60 * 2
