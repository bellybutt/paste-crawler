import time

import consts
from crawler import Crawler
from logger import logger


def run():
    crawler = Crawler()
    try:
        while True:
            logger.debug("Started crawl")
            crawler.crawl()
            logger.debug(f"Done crawl \n "
                         f"Going to sleep for {consts.SECONDS_TO_SLEEP / 60} minutes")
            time.sleep(consts.SECONDS_TO_SLEEP)
    except Exception:
        logger.exception(f"Crawl failed \n "
                         f"Going to sleep for {consts.SECONDS_TO_SLEEP / 60} minutes")
        time.sleep(consts.SECONDS_TO_SLEEP)


if __name__ == '__main__':
    run()
